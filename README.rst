=======
ArtnExp
=======

Artnexp is a simple Django app for managing a publication list and the
experiments linked to each article.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "artnexp" to your INSTALLED_APPS setting like this:

    INSTALLED_APPS = [
        ...
        'artnexp',
    ]

2. Include the agenda URLconf in your project urls.py like this:

    url(r'^artnexp/', include('artnexp.urls',namespace='artnexp')),

3. Run `python manage.py migrate` to create the publications models.

4. Start the development server and visit http://127.0.0.1:8000/artnexp/ to use
   the app
