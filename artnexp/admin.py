from django.contrib import admin
from .models import Experiment,Article,Author,Publication,Authored

class ExperimentInline(admin.StackedInline):
    model = Experiment
    extra = 1

class PublicationInline(admin.StackedInline):
    model = Publication
    extra = 1

class AuthorInline(admin.StackedInline):
    model = Authored
    extra = 1

class ArticleAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Informations Générales",               {'fields': ['title','description','article']}),
    ]
    inlines = [AuthorInline,PublicationInline,ExperimentInline]


admin.site.register(Author)
admin.site.register(Article,ArticleAdmin)
#admin.site.register(Article)

# Register your models here.
