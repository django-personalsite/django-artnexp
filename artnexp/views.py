from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render

from .models import Article
# Create your views here.


def index(request):
     article_list = Article.objects.order_by('-id')

     context = {
        'article_list' : article_list,
     }
     return render(request, 'artnexp/index.html', context)
 

def detail(request, article_id):
     article=get_object_or_404(Article, pk=article_id)
     context = {
        'article' : article,
     }
     return render(request, 'artnexp/detail.html', context)

