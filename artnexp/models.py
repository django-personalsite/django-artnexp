from django.db import models



class Author(models.Model):
    firstname=models.CharField(max_length=200)
    lastname=models.CharField(max_length=200)
    email=models.EmailField(blank=True)
    university = models.CharField(max_length=100,blank=True)
    web_address = models.URLField(blank=True)
    
    def __str__(self):
        return self.firstname+" "+self.lastname
    class Meta:
        ordering = ['lastname']

class Article(models.Model):
    title=models.CharField(max_length=200)
    description=models.TextField()
    article=models.FileField(upload_to="articles",blank=True)
    conference=models.CharField(max_length=200)

    def __str__(self):
        return self.title

class Authored(models.Model):
    AUTHOR_NO = ((1, "1"), (2, "2"), (3, "3"), (4, "4"), (5, "5"), (6, "6"), (7,
        "7"),(8,"8"),(9,"9"),(10,"10+"))

    author = models.ForeignKey(Author)
    article = models.ForeignKey(Article)
    author_ord = models.IntegerField(choices=AUTHOR_NO,verbose_name="Order")

    class Meta:
        ordering = ['author_ord']
        verbose_name = "Ordered Author"



class Experiment(models.Model):
    title=models.CharField(max_length=200)
    sections=models.CharField(max_length=200,blank=True)
    archive=models.FileField(upload_to="experiments",blank=True)
    description=models.TextField(blank=True)
    requirements=models.TextField(blank=True)
    howto=models.TextField(blank=True)
    article=models.ForeignKey(Article)

    def __str__(self):
        return self.title

class Publication(models.Model):
    PUBLICATIONS=(
            ('1', 'Unpublished'),
            ('2', 'Conference'),
            ('3', 'Journal'),
            ('4', 'Technical Report'),
            ('5', 'Other')
            )
    STATUS=(
            ('1', 'Unpublished'),
            ('2', 'Published'),
            ('3', 'Accepted'),
            ('4', 'Submitted'),
            ('5', 'Other')
            )
    pubtype=models.CharField(max_length=1,choices=PUBLICATIONS,default='1')
    pubstatus=models.CharField(max_length=1,choices=STATUS,default='1',blank=True)
    name=models.CharField(max_length=200,blank=True)
    url=models.URLField(blank=True)
    more=models.CharField(max_length=200,blank=True)
    date=models.DateField(blank=True)
    article=models.OneToOneField(Article)

    def __str__(self):
        return self.name
